import pygame


class Corn:
    corn_image = "images/small_corn"

    def __init__(self):
        self.image = pygame.image.load(self.corn_image)
        self.geometry = self.image.get_rect()
