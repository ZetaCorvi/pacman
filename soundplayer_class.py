import pygame
import settings


class SoundPlayer:
    def __init__(self, link, type_of_sound):
        self.sound = pygame.mixer.Sound(link)
        self.type_of_sound = type_of_sound
        self.volume = 0
        self.set_volume()

    def play(self):
        self.set_volume()
        self.sound.set_volume(self.volume)
        if self.type_of_sound == 'music':
            self.sound.play(loops=-1)
        else:
            self.sound.play()

    def stop(self):
        self.sound.stop()

    def set_volume(self):
        if self.type_of_sound == 'music':
            self.volume = settings.MUSIC_VOLUME / 100
        else:
            self.volume = settings.EFFECTS_VOLUME / 100
