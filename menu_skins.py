from interface_elements import *
import settings
import pygame
from soundplayer_class import SoundPlayer
import rus
import eng


class SkinsMenu:
    def __init__(self):
        self.lang = None
        if settings.SELECTED_LANGUAGE == "rus":
            self.lang = rus
        else:
            self.lang = eng
        self.images = dict()
        self.buttons = dict()
        self.skins = list()
        self.menu_left = False
        self.create_skins_panel()
        self.title_text = self.lang.skins
        self.screen = pygame.display.set_mode([WIDTH, HEIGHT])
        message_qt = self.lang.back
        tmp_qt = Button((0, 0, 150, 100), BLACK, self.go_back_tostart_panel, text=message_qt, **BUTTON_STYLE)
        tmp_qt.rect.center = (WIDTH / 2, HEIGHT - HEIGHT // 9)
        self.back_button = tmp_qt
        tmp_img = Image(WIDTH / 2 - 178, -30, 'images/pacman_title.png', True)
        self.images['title_image'] = tmp_img

    def main_loop(self):
        while not self.menu_left:
            self.check_events()
            self.draw_all()

    def create_skins_panel(self):
        i = 1
        while True:
            try:
                self.skins.append(ButtonSkin("images/skins/{}/right.png".format(i), i, self.skins, True, int(settings.PACMAN_SKIN) == i))
            except pygame.error:
                break
            i += 1

    def check_events(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            for i in self.buttons.values():
                if i.active:
                    i.check_event(event)
            if self.back_button and self.back_button.active:
                self.back_button.check_event(event)
            for i in self.skins:
                if i.active:
                    i.check_event(event)

    def draw_all(self):
        self.screen.fill(BLACK)
        for i in self.images.values():
            if i:
                if type(i) == Image and i.active:
                    self.screen.blit(i.image, i.geometry)
                elif type(i) == list:
                    for j in i:
                        if j:
                            if j.active:
                                self.screen.blit(j.image, j.geometry)
        for i in self.skins:
            if i.active:
                i.check_animation()
                self.screen.blit(i.image.image, i.image.geometry)
        for i in self.buttons.values():
            if type(i) == Button and i.active:
                i.update(self.screen)
        if self.back_button and self.back_button.active:
            self.back_button.update(self.screen)
        self.draw_text()
        pygame.display.flip()

    def go_back_tostart_panel(self):
        for i in self.buttons.values():
            i.active = True
        self.back_button.active = False
        self.title_text = None
        self.menu_left = True

    def draw_text(self):
        font = pygame.font.SysFont('Comic Sans MS', 30)
        if self.title_text:
            size = font.size(self.title_text)
            surface = font.render(self.title_text, False, WHITE)
            self.screen.blit(surface, (WIDTH // 2 - size[0] // 2, HEIGHT / 6))


class ButtonSkin:
    def __init__(self, image_link, image_num, skins_list, active=False, chosen=False):
        image_num -= 1
        self.image = Image((WIDTH // 4 + image_num % 3 * WIDTH // 4) - round(50 * 672 / WIDTH) // 2, HEIGHT // 3.2 + image_num // 3 * WIDTH // 4, image_link, True)
        self.image.image = pygame.transform.scale(self.image.image, (round(50 * 672 / WIDTH), round(50 * 672 / WIDTH)))
        self.active = active
        self.chosen = chosen
        self.click_s = SoundPlayer("sound/mouse_click.aif", "effect")
        self.clicked = False
        self.rect = pygame.Rect((0, 0, 120, 120))
        self.rect.center = self.image.geometry.center
        self.skin_num = image_num + 1
        self.skins_list = skins_list
        self.prev_chosen = False

    def check_event(self, event):
        """The button needs to be passed events from your program event loop."""
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.on_click(event)
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.on_release(event)

    def on_click(self, event):
        if self.rect.collidepoint(event.pos):
            self.clicked = True
            self.click_s.play()

    def on_release(self, event):
        if self.clicked:
            settings.PACMAN_SKIN = str(self.skin_num)
            for i in self.skins_list:
                i.chosen = False
            self.chosen = True
            self.clicked = False

    def check_animation(self):
        if self.chosen:
            self.image.change_image("images/skins/{}/stop.png".format(self.skin_num))
            self.image.image = pygame.transform.scale(self.image.image, (round(80 * 672 / WIDTH), round(80 * 672 / WIDTH)))
        else:
            self.image.change_image("images/skins/{}/right.png".format(self.skin_num))
            self.image.image = pygame.transform.scale(self.image.image, (round(50 * 672 / WIDTH), round(50 * 672 / WIDTH)))
        self.prev_chosen = self.chosen
