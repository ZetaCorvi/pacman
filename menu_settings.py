from interface_elements import *
from settings import *
import settings
import rus
import eng
from easygui import *


class SettingsMenu:
    def __init__(self):
        self.images = dict()
        self.buttons = dict()
        self.menu_left = False
        self.screen = pygame.display.set_mode([WIDTH, HEIGHT])
        self.back_button = None
        self.title_text = None
        self.lang = None
        if settings.SELECTED_LANGUAGE == "rus":
            self.lang = rus
        else:
            self.lang = eng

    def main_loop(self):
        self.create_settings_panel()
        while not self.menu_left:
            self.check_events()
            self.draw_all()

    def create_settings_panel(self):

        message_volp = self.lang.music_vol + " +"
        tmp_volp = Button((0, 0, 270, 80), BLACK, self.VolUp, text=message_volp, **BUTTON_STYLE)
        tmp_volp.rect.center = (WIDTH / 4 * 3, 320)
        self.buttons['volup_btn'] = tmp_volp

        message_volm = self.lang.music_vol + " -"
        tmp_volm = Button((0, 0, 270, 80), BLACK, self.VolDn, text=message_volm, **BUTTON_STYLE)
        tmp_volm.rect.center = (WIDTH / 4, 320)
        self.buttons['voldn_btn'] = tmp_volm

        message_sfxp = self.lang.sfx_vol + " +"
        tmp_sp = Button((0, 0, 270, 80), BLACK, self.SFXUp, text=message_sfxp, **BUTTON_STYLE)
        tmp_sp.rect.center = (WIDTH / 4 * 3, 420)
        self.buttons['sfxup_btn'] = tmp_sp

        message_sfxm = self.lang.sfx_vol + " -"
        tmp_sm = Button((0, 0, 270, 80), BLACK, self.SFXDn, text=message_sfxm, **BUTTON_STYLE)
        tmp_sm.rect.center = (WIDTH / 4, 420)
        self.buttons['sfxdn_btn'] = tmp_sm

        message_qt = self.lang.back
        tmp_qt = Button((0, 0, 150, 100), BLACK, self.qt, text=message_qt, **BUTTON_STYLE)
        tmp_qt.rect.center = (WIDTH / 2, 620)
        self.buttons['quit_btn'] = tmp_qt

        message_lang = self.lang.lang_in_settings
        tmp_lang = Button((0, 0, 300, 80), BLACK, self.change_lang, text=message_lang, **BUTTON_STYLE)
        tmp_lang.rect.center = (WIDTH / 2, 220)
        self.buttons['lang_btn'] = tmp_lang

        tmp_img = Image(WIDTH / 2 - 178, -30, 'images/pacman_title.png', True)
        self.images['title_image'] = tmp_img

    def change_lang(self):
        if settings.SELECTED_LANGUAGE == "rus":
            settings.SELECTED_LANGUAGE = "eng"
            self.lang = eng
        else:
            settings.SELECTED_LANGUAGE = "rus"
            self.lang = rus
        self.create_settings_panel()
        f = open("settings.txt", 'w')
        f.write(settings.SELECTED_LANGUAGE + " " + str(settings.MUSIC_VOLUME) + " " + str(settings.EFFECTS_VOLUME))
        f.close()

    @staticmethod
    def save_changes():
        f = open("settings.txt", 'w')
        f.write(settings.SELECTED_LANGUAGE + " " + str(settings.MUSIC_VOLUME) + " " + str(settings.EFFECTS_VOLUME))
        f.close()

    def VolUp(self):
        if settings.MUSIC_VOLUME < 100:
            settings.MUSIC_VOLUME += 1
        self.save_changes()

    def VolDn(self):
        if settings.MUSIC_VOLUME > 0:
            settings.MUSIC_VOLUME -= 1
        self.save_changes()

    def SFXUp(self):
        if settings.EFFECTS_VOLUME < 100:
            settings.EFFECTS_VOLUME += 1
        self.save_changes()

    def SFXDn(self):
        if settings.EFFECTS_VOLUME > 0:
            settings.EFFECTS_VOLUME -= 1
        self.save_changes()

    def qt(self):
        self.menu_left = True
        for i in self.buttons.values():
            i.active = False

    def check_events(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            for i in self.buttons.values():
                if i.active:
                    i.check_event(event)
            if self.back_button and self.back_button.active:
                self.back_button.check_event(event)

    def draw_all(self):
        self.screen.fill(BLACK)
        for i in self.images.values():
            if i:
                if type(i) == Image and i.active:
                    self.screen.blit(i.image, i.geometry)
                elif type(i) == list:
                    for j in i:
                        if j:
                            if j.active:
                                self.screen.blit(j.image, j.geometry)
        for i in self.buttons.values():
            if type(i) == Button and i.active:
                i.update(self.screen)
        if self.back_button and self.back_button.active:
            self.back_button.update(self.screen)
        self.draw_text()
        pygame.display.flip()

    def draw_text(self):
        font = pygame.font.SysFont('Comic Sans MS', 30)
        text = self.lang.settings
        tmp = font.size(text)
        surface = font.render(text, False, WHITE)
        self.screen.blit(surface, (WIDTH // 2 - tmp[0] // 2, HEIGHT // 6))

        text = "{}".format(settings.MUSIC_VOLUME)
        size = font.size(text)
        surface = font.render(text, False, WHITE)
        self.screen.blit(surface, (WIDTH // 2 - size[0] // 2, 320))

        text = "{}".format(settings.EFFECTS_VOLUME)
        size = font.size(text)
        surface = font.render(text, False, WHITE)
        self.screen.blit(surface, (WIDTH // 2 - size[0] // 2, 420))
