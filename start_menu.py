from menu_settings import *
from menu_skins import SkinsMenu
import pygame
import rus 
import eng
import settings


class StartMenu:
    def __init__(self, info_structure):
        self.images = dict()
        self.buttons = dict()
        self.game_started = False
        self.goto_settings = False
        self.goto_highscores = False
        self.screen = pygame.display.set_mode(SIZE)
        self.highscores_list = list()
        self.back_button = None
        self.main_text = None
        self.title_text = None
        self.info_structure = info_structure
        self.buttons_game_mode = dict()
        self.lang = None
        if settings.SELECTED_LANGUAGE == "rus":
            self.lang = rus
        else:
            self.lang = eng

    def main_loop(self):
        self.create_start_panel()
        while not self.game_started:
            self.check_events()
            if self.lang.name_of_lang != settings.SELECTED_LANGUAGE:
                if settings.SELECTED_LANGUAGE == "rus":
                    self.lang = rus
                else:
                    self.lang = eng
                self.create_start_panel()
            self.draw_all()

    @staticmethod
    def quit():
        exit()

    def create_start_panel(self):
        tmp_img = Image(WIDTH / 2 - 178, -30, 'images/pacman_title.png', True)
        self.images['title_image'] = tmp_img
        message = self.lang.start
        tmp_btn = Button((0, 0, 280, 80), BLACK, self.start_game, text=message, **BUTTON_STYLE)
        tmp_btn.rect.center = (WIDTH / 2,  HEIGHT // 10 + 100)
        self.buttons['start_btn'] = tmp_btn

        message = self.lang.settings
        tmp_stt = Button((0, 0, 280, 80), BLACK, self.settings_menu, text=message, **BUTTON_STYLE)
        tmp_stt.rect.center = (WIDTH / 2, HEIGHT // 10 * 6 + 100)
        self.buttons['settings_btn'] = tmp_stt

        message = self.lang.highscores
        tmp_scr = Button((0, 0, 280, 80), BLACK, self.highscores_menu, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 10 * 2 + 100)
        self.buttons['highscores_btn'] = tmp_scr

        message = self.lang.skins
        tmp_scr = Button((0, 0, 280, 80), BLACK, self.skins_menu, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 10 * 4 + 100)
        self.buttons['skins_btn'] = tmp_scr

        message = self.lang.game_mode
        tmp_scr = Button((0, 0, 280, 80), BLACK, self.game_mode, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 10 * 3 + 100)
        self.buttons['game_mode'] = tmp_scr

        message = self.lang.quit
        tmp_qt = Button((0, 0, 280, 80), BLACK, exit, text=message, **BUTTON_STYLE)
        tmp_qt.rect.center = (WIDTH / 2, HEIGHT // 10 * 7 + 100)
        self.buttons['quit_btn'] = tmp_qt

        message = self.lang.tutorial
        tmp_stt = Button((0, 0, 280, 80), BLACK, self.tutorial, text=message, **BUTTON_STYLE)
        tmp_stt.rect.center = (WIDTH / 2, HEIGHT // 10 * 5 + 100)
        self.buttons['tutorial_btn'] = tmp_stt

    def settings_menu(self):
        settings_menu = SettingsMenu()
        settings_menu.main_loop()

    def skins_menu(self):
        skins_menu = SkinsMenu()
        skins_menu.main_loop()

    def highscores_menu(self):
        self.highscores()

    def start_game(self):
        self.game_started = True

    def check_events(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            for i in self.buttons.values():
                if i.active:
                    i.check_event(event)
            for i in self.buttons_game_mode.values():
                if i.active:
                    i.check_event(event)
            if self.back_button and self.back_button.active:
                self.back_button.check_event(event)

    def draw_all(self):
        self.screen.fill(BLACK)
        for i in self.images.values():
            if i:
                if type(i) == Image and i.active:
                    self.screen.blit(i.image, i.geometry)
                elif type(i) == list:
                    for j in i:
                        if j:
                            if j.active:
                                self.screen.blit(j.image, j.geometry)
        for i in self.buttons.values():
            if type(i) == Button and i.active:
                i.update(self.screen)
        for i in self.buttons_game_mode.values():
            if type(i) == Button and i.active:
                i.update(self.screen)
        if self.back_button and self.back_button.active:
            self.back_button.update(self.screen)
        self.draw_text()
        pygame.display.flip()

    def game_mode(self):
        self.title_text = self.lang.game_mode
        message_scores = self.lang.back
        self.back_button = Button((0, 0, 250, 80), BLACK, self.go_back_tostart_panel, text=message_scores, **BUTTON_STYLE)
        self.back_button.rect.center = (WIDTH / 2, HEIGHT - HEIGHT / 10)
        for i in self.buttons.values():
            i.active = False
        message = self.lang.easy
        tmp_scr = Button((0, 0, 250, 80), BLACK, self.set_easy_level, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 6 + HEIGHT // 10 * 1)
        self.buttons_game_mode['easy'] = tmp_scr

        message = self.lang.medium
        tmp_scr = Button((0, 0, 250, 80), BLACK, self.set_med_level, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 6 + HEIGHT // 10 * 2)
        self.buttons_game_mode['medium'] = tmp_scr

        message = self.lang.hard
        tmp_scr = Button((0, 0, 250, 80), BLACK, self.set_hard_level, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 6 + HEIGHT // 10 * 3)
        self.buttons_game_mode['hard'] = tmp_scr

        message = self.lang.no_pacman
        tmp_scr = Button((0, 0, 250, 80), BLACK, self.set_no_pacman_level, text=message, **BUTTON_STYLE)
        tmp_scr.rect.center = (WIDTH / 2, HEIGHT // 6 + HEIGHT // 10 * 4)
        self.buttons_game_mode['no_pacman'] = tmp_scr

        self.images["ok"] = Image(WIDTH // 2 - WIDTH // 7, HEIGHT // 6 + HEIGHT // 10 * 2, "images/ok.png", True)
        self.images["ok"].image = pygame.transform.scale(self.images["ok"].image, (30, 30))
        self.images["ok"].geometry.center = (WIDTH // 2 - WIDTH // 7, HEIGHT // 6 + HEIGHT // 10 * self.info_structure.game_mode + HEIGHT // 15)


    def set_easy_level(self):
        self.info_structure.wave_cow = 2
        self.info_structure.last_time_tic = 20000
        self.info_structure.pacman_speed = 4
        self.info_structure.ghost_speed = 2
        self.info_structure.no_pacman_mode = False
        self.info_structure.game_mode = 1
        self.images["ok"].geometry.center = (WIDTH // 2 - WIDTH // 7, HEIGHT // 6 + HEIGHT // 10 * 1 + HEIGHT // 15)

    def set_med_level(self):
        self.info_structure.wave_cow = 4
        self.info_structure.last_time_tic = 20000
        self.info_structure.pacman_speed = 3
        self.info_structure.ghost_speed = 2
        self.info_structure.no_pacman_mode = False
        self.info_structure.game_mode = 2
        self.images["ok"].geometry.center = (WIDTH // 2 - WIDTH // 7, HEIGHT // 6 + HEIGHT // 10 * 2 + HEIGHT // 15)

    def set_hard_level(self):
        self.info_structure.wave_cow = 7
        self.info_structure.last_time_tic = 20000
        self.info_structure.pacman_speed = 3
        self.info_structure.ghost_speed = 3
        self.info_structure.no_pacman_mode = False
        self.info_structure.game_mode = 3
        self.images["ok"].geometry.center = (WIDTH // 2 - WIDTH // 7, HEIGHT // 6 + HEIGHT // 10 * 3 + HEIGHT // 15)

    def set_no_pacman_level(self):
        self.info_structure.wave_cow = 3
        self.info_structure.last_time_tic = 20000
        self.info_structure.pacman_speed = 3
        self.info_structure.ghost_speed = 2
        self.info_structure.no_pacman_mode = True
        self.info_structure.game_mode = 4
        self.images["ok"].geometry.center = (WIDTH // 2 - WIDTH // 7, HEIGHT // 6 + HEIGHT // 10 * 4 + HEIGHT // 15)

    def go_back_tostart_panel(self):
        for i in self.buttons.values():
            i.active = True
        self.back_button.active = False
        self.main_text = None
        self.title_text = None
        self.highscores_list.clear()
        self.back_button.active = False
        self.images["tutorial"] = None
        self.buttons_game_mode.clear()
        self.images["ok"] = None
        
    def tutorial(self):
        for i in self.buttons.values():
            i.active = False
        self.images['tutorial'] = Image(WIDTH // 2 - 200, HEIGHT // 2 - 180, 'images/tutorial.png', True)
        message_scores = "BACK"
        self.back_button = Button((0, 0, 150, 100), BLACK, self.go_back_tostart_panel, text=message_scores, **BUTTON_STYLE)
        self.back_button.rect.center = (WIDTH / 2, HEIGHT - HEIGHT / 10)
        self.title_text = self.lang.tutorial

    def highscores(self):
        for i in self.buttons.values():
            i.active = False
        try:
            highscores_file = open("highscores.txt")
            for line in highscores_file:
                if len(line[:-1].split()):
                    self.highscores_list.append(str(line[:-1]))
        except FileNotFoundError:
            pass
        self.sort_highscores()
        if len(self.highscores_list) > 10:
            self.highscores_list = self.highscores_list[:10]
        message_scores = self.lang.back
        self.back_button = Button((0, 0, 150, 100), BLACK, self.go_back_tostart_panel, text=message_scores, **BUTTON_STYLE)
        self.back_button.rect.center = (WIDTH / 2, HEIGHT / 4 + (len(self.highscores_list) + 1) * (HEIGHT / 16.5))
        self.title_text = self.lang.highscores

    def sort_highscores(self):
        for j in self.highscores_list:
            for i in range(len(self.highscores_list) - 1):
                name, score = self.highscores_list[i].split()
                name1, score1 = self.highscores_list[i + 1].split()
                score = int(score)
                score1 = int(score1)
                if score1 > score:
                    self.highscores_list[i + 1], self.highscores_list[i] = self.highscores_list[i], self.highscores_list[i + 1]

    def draw_text(self):
        font = pygame.font.SysFont('Comic Sans MS', 30)
        if self.title_text:
            size = font.size(self.title_text)
            surface = font.render(self.title_text, False, WHITE)
            self.screen.blit(surface, (WIDTH // 2 - size[0] // 2, HEIGHT / 6))
        if len(self.highscores_list) > 0:
            for i in range(len(self.highscores_list)):
                name, score = self.highscores_list[i].split()
                if len(name) > 20:
                    name = name[:20] + "..."
                font = pygame.font.SysFont('Arial', 30)
                surface = font.render("№{} ".format(i + 1), False, RED)
                self.screen.blit(surface, (WIDTH / 6, HEIGHT / 4 + i * (HEIGHT / 16)))
                surface = font.render(score, False, BLUE)
                self.screen.blit(surface, (WIDTH - WIDTH / 4, HEIGHT / 4 + i * (HEIGHT / 16)))
                surface = font.render(name, False, WHITE)
                self.screen.blit(surface, (WIDTH / 6 + WIDTH / 10, HEIGHT / 4 + i * (HEIGHT / 16)))
