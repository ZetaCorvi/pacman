from ghost_base import BaseGhost
from interface_elements import Image
from math import sqrt
from settings import *
import settings


class Blinky(BaseGhost):
    def get_target_point(self):
        if self.movement_mode == "chase":
            self.target_point = self.pacman.cell_x, self.pacman.cell_y
        elif self.movement_mode == "scatter":
            self.target_point = 30, -2
        elif self.movement_mode == "going_home":
            self.target_point = BLINKY_SPAWN_POINT[0] / SQUARE_SIZE, BLINKY_SPAWN_POINT[1] / SQUARE_SIZE

    def get_animation(self):
        self.animation_num += 1
        if self.animation_num >= 5:
            self.animation_num = 3
        if self.movement_mode == "chase" or self.movement_mode == "scatter" or self.movement_mode == "home" or self.movement_mode == "going_from_home":
            self.sprite.change_image("images/blinky_{}_{}.png".format(self.direction, self.animation_num))
        elif self.movement_mode == "frightened":
            if settings.SCALED_TIME - self.pacman.eating_mode_started >= self.pacman.eating_mode_is_active_for - 3000:
                if self.white_and_blue_anim_num == 1:
                    self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
                elif self.white_and_blue_anim_num == 0:
                    self.sprite.change_image("images/white_{}.png".format(self.animation_num))
                if settings.SCALED_TIME - self.white_and_blue_changed_time >= 500:
                    self.white_and_blue_anim_num += 1
                    self.white_and_blue_anim_num %= 2
                    self.white_and_blue_changed_time = settings.SCALED_TIME
            else:
                self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
        elif self.movement_mode == "going_home":
            self.sprite.change_image("images/eyes_{}.png".format(self.direction))


class Pinky(BaseGhost):
    def get_target_point(self):
        if self.movement_mode == "chase":
            if self.pacman.direction == "up":
                self.target_point = self.pacman.cell_x, self.pacman.cell_y - 4
            elif self.pacman.direction == "down":
                self.target_point = self.pacman.cell_x, self.pacman.cell_y + 4
            elif self.pacman.direction == "left":
                self.target_point = self.pacman.cell_x - 4, self.pacman.cell_y
            elif self.pacman.direction == "right":
                self.target_point = self.pacman.cell_x + 4, self.pacman.cell_y
        elif self.movement_mode == "scatter":
            self.target_point = -2, 0
        elif self.movement_mode == "going_home":
            self.target_point = BLINKY_SPAWN_POINT[0] / SQUARE_SIZE, BLINKY_SPAWN_POINT[1] / SQUARE_SIZE

    def get_animation(self):
        self.animation_num += 1
        if self.animation_num >= 5:
            self.animation_num = 3
        if self.movement_mode == "chase" or self.movement_mode == "scatter" or self.movement_mode == "home" or self.movement_mode == "going_from_home":
            self.sprite.change_image("images/pinky_{}_{}.png".format(self.direction, self.animation_num))
        elif self.movement_mode == "frightened":
            if settings.SCALED_TIME - self.pacman.eating_mode_started >= self.pacman.eating_mode_is_active_for - 3000:
                if self.white_and_blue_anim_num == 1:
                    self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
                elif self.white_and_blue_anim_num == 0:
                    self.sprite.change_image("images/white_{}.png".format(self.animation_num))
                if settings.SCALED_TIME - self.white_and_blue_changed_time >= 500:
                    self.white_and_blue_anim_num += 1
                    self.white_and_blue_anim_num %= 2
                    self.white_and_blue_changed_time = settings.SCALED_TIME
            else:
                self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
        elif self.movement_mode == "going_home":
            self.sprite.change_image("images/eyes_{}.png".format(self.direction))


class Inky(BaseGhost):
    def __init__(self, x, y, image_link, field, pacman, blinky, ghost_type, info_struct):
        self.sprite = Image(x, y, image_link, True, 2, "game_object")
        self.field = field
        self.geometry = self.sprite.geometry
        self.pacman = pacman
        self.speed = info_struct.ghost_speed
        self.info_struct = info_struct
        self.active = True
        self.direction = "right"
        self.cell_y = x // SQUARE_SIZE
        self.cell_x = y // SQUARE_SIZE
        self.last_cell_x = x // SQUARE_SIZE
        self.last_cell_y = y // SQUARE_SIZE
        self.is_cell_checked = False
        self.target_point = 10, 10
        self.blinky = blinky
        self.movement_mode = "home"
        self.name = ghost_type
        self.animation_num = 3
        self.last_time = 0
        self.white_and_blue_anim_num = 1
        self.white_and_blue_changed_time = 0

    def get_target_point(self):
        if self.movement_mode == "chase":
            if self.pacman.direction == "up":
                self.target_point = self.pacman.cell_x, self.pacman.cell_y - 2
            if self.pacman.direction == "down":
                self.target_point = self.pacman.cell_x, self.pacman.cell_y + 2
            if self.pacman.direction == "left":
                self.target_point = self.pacman.cell_x - 2, self.pacman.cell_y
            if self.pacman.direction == "right":
                self.target_point = self.pacman.cell_x + 2, self.pacman.cell_y
            self.target_point = (self.target_point[0] - self.blinky.cell_x) * 2 + self.blinky.cell_x, \
                                (self.target_point[1] - self.blinky.cell_y) * 2 + self.blinky.cell_y
        elif self.movement_mode == "scatter":
            self.target_point = 30, 35
        elif self.movement_mode == "going_home":
            self.target_point = BLINKY_SPAWN_POINT[0] / SQUARE_SIZE, BLINKY_SPAWN_POINT[1] / SQUARE_SIZE

    def get_animation(self):
        self.animation_num += 1
        if self.animation_num >= 5:
            self.animation_num = 3
        if self.movement_mode == "chase" or self.movement_mode == "scatter" or self.movement_mode == "home" or self.movement_mode == "going_from_home":
            self.sprite.change_image("images/inky_{}_{}.png".format(self.direction, self.animation_num))
        elif self.movement_mode == "frightened":
            if settings.SCALED_TIME - self.pacman.eating_mode_started >= self.pacman.eating_mode_is_active_for - 3000:
                if self.white_and_blue_anim_num == 1:
                    self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
                elif self.white_and_blue_anim_num == 0:
                    self.sprite.change_image("images/white_{}.png".format(self.animation_num))
                if settings.SCALED_TIME - self.white_and_blue_changed_time >= 500:
                    self.white_and_blue_anim_num += 1
                    self.white_and_blue_anim_num %= 2
                    self.white_and_blue_changed_time = settings.SCALED_TIME
            else:
                self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
        elif self.movement_mode == "going_home":
            self.sprite.change_image("images/eyes_{}.png".format(self.direction))


class Clyde(BaseGhost):
    def get_target_point(self):
        if self.movement_mode == "chase":
            distance = sqrt(pow(self.cell_x - self.pacman.cell_x, 2) + pow(self.cell_y - self.pacman.cell_y, 2))
            if distance >= 8:
                self.target_point = self.pacman.cell_x, self.pacman.cell_y
            else:
                self.target_point = 0, 34
        elif self.movement_mode == "scatter":
            self.target_point = -2, 35
        elif self.movement_mode == "going_home":
            self.target_point = BLINKY_SPAWN_POINT[0] / SQUARE_SIZE, BLINKY_SPAWN_POINT[1] / SQUARE_SIZE

    def get_animation(self):
        self.animation_num += 1
        if self.animation_num >= 5:
            self.animation_num = 3
        if self.movement_mode == "chase" or self.movement_mode == "scatter" or self.movement_mode == "home" or self.movement_mode == "going_from_home":
            self.sprite.change_image("images/clyde_{}_{}.png".format(self.direction, self.animation_num))
        elif self.movement_mode == "frightened":
            if settings.SCALED_TIME - self.pacman.eating_mode_started >= self.pacman.eating_mode_is_active_for - 3000:
                if self.white_and_blue_anim_num == 1:
                    self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
                elif self.white_and_blue_anim_num == 0:
                    self.sprite.change_image("images/white_{}.png".format(self.animation_num))
                if settings.SCALED_TIME - self.white_and_blue_changed_time >= 500:
                    self.white_and_blue_anim_num += 1
                    self.white_and_blue_anim_num %= 2
                    self.white_and_blue_changed_time = settings.SCALED_TIME
            else:
                self.sprite.change_image("images/blue_{}.png".format(self.animation_num))
        elif self.movement_mode == "going_home":
            self.sprite.change_image("images/eyes_{}.png".format(self.direction))

