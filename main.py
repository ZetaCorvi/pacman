import pygame

import settings
from game import Game
from game_over import GameOver
from start_menu import StartMenu
from structs import GameStructure

if __name__ == "__main__":
    lines_of_settings = list()
    f = open("settings.txt")
    for line in f:
        lines_of_settings.append(line)
    tmp = lines_of_settings[0].split()
    settings.SELECTED_LANGUAGE = tmp[0]
    settings.MUSIC_VOLUME = int(tmp[1])
    settings.EFFECTS_VOLUME = int(tmp[2])
    f.close()
    pygame.init()
    pygame.font.init()
    pygame.display.set_caption("Pacman")
    info_structure = GameStructure()
    start_menu = StartMenu(info_structure)
    start_menu.main_loop()
    g = Game(info_structure)
    g.main_loop()
    game_over_screen = GameOver(info_structure)
    game_over_screen.main_loop()
    while True:
        if game_over_screen.should_go_to == "main menu":
            start_menu = StartMenu(info_structure)
            start_menu.main_loop()
            g = Game(info_structure)
            g.main_loop()
            game_over_screen = GameOver(info_structure)
            game_over_screen.main_loop()
        elif game_over_screen.should_go_to == "game":
            g = Game(info_structure)
            g.main_loop()
            game_over_screen = GameOver(info_structure)
            game_over_screen.main_loop()
    exit()
