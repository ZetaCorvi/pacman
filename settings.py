SIZE = WIDTH, HEIGHT = 672, 800  # Размеры экрана
SQUARE_SIZE = 24
PACMAN_SIZE = 36
SCALED_TIME = 0
UNSCALED_TIME = 0
TIMESCALE = 1
##############################################################################
SQUARE_SIZE_D = 24
PACMAN_SIZE_D = 36
DEFAULTS = WIDTH_D, HEIGHT_D = 672, 800
FIELD_IMAGE_SIZE = FIELD_SIZE_X, FIELD_SIZE_Y = 672, 744
#####################################################################################
TELEPORT_CELL_Y = 14
BLINKY_SPAWN_POINT = BLINKY_SPAWN_X, BLINKY_SPAWN_Y = 318, 258
PACMAN_SPAWN_POINT = PACMAN_SPAWN_X, PACMAN_SPAWN_Y = 318, 546
PINKY_SPAWN_POINT = PINKY_SPAWN_X, PINKY_SPAWN_Y = 318, 331
INKY_SPAWN_POINT = INKY_SPAWN_X, INKY_SPAWN_Y = 270, 331
CLYDE_SPAWN_POINT = CLYDE_SPAWN_X, CLYDE_SPAWN_Y = 366, 331
SIZE_CELLS = WIDTH_CELLS, HEIGHT_CELLS = 28, 31
PACMAN_SKIN = "5"
#####################################################################################
MUSIC_VOLUME = 100
EFFECTS_VOLUME = 100
#####################################################################################
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
ORANGE = (255, 180, 0)
#####################################################################################
BUTTON_STYLE = {"hover_color": BLACK,
                "clicked_color": (173, 100, 34),
                "clicked_font_color": BLACK,
                "hover_font_color": BLUE}
#################################################################################
SELECTED_LANGUAGE = "rus"
