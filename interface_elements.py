import pygame
from soundplayer_class import SoundPlayer
from settings import *


class Image(object):
    def __init__(self, pos_x, pos_y, image_link, active=False, layer=0, type_of_image="default"):
        self.image = pygame.image.load(image_link)
        self.geometry = self.image.get_rect()
        self.geometry.left = pos_x
        self.geometry.top = pos_y
        self.active = active
        self.layer = layer
        self.type_of_image = type_of_image
        if type_of_image == "game_object":
            self.image = pygame.transform.scale(self.image, (PACMAN_SIZE, PACMAN_SIZE))
        elif type_of_image == "field":
            self.image = pygame.transform.scale(self.image, (FIELD_SIZE_X, FIELD_SIZE_Y))
        elif type_of_image == "field_object":
            self.image = pygame.transform.scale(self.image, (SQUARE_SIZE, SQUARE_SIZE))

    def change_image(self, image_link):
        geometry = self.geometry
        self.image = pygame.image.load(image_link)
        self.geometry = geometry
        if self.type_of_image == "game_object":
            self.image = pygame.transform.scale(self.image, (PACMAN_SIZE, PACMAN_SIZE))
        elif self.type_of_image == "field":
            self.image = pygame.transform.scale(self.image, (FIELD_SIZE_X, FIELD_SIZE_Y))

class Button(object):
    """A fairly straight forward button class."""
    def __init__(self,rect,color,function, transparent=False, **kwargs):
        self.click_s = SoundPlayer("sound/mouse_click.aif", "effect")
        self.rect = pygame.Rect(rect)
        self.color = color
        self.function = function
        self.clicked = False
        self.hovered = False
        self.hover_text = None
        self.clicked_text = None
        self.transparent = transparent
        self.process_kwargs(kwargs)
        self.render_text()
        self.active = True

    def process_kwargs(self, kwargs):
        """Various optional customization you can change by passing kwargs."""
        settings = {"text" : None,
                    "font" : pygame.font.SysFont("Comic Sans MS", 30),
                    "call_on_release" : True,
                    "hover_color" : None,
                    "clicked_color" : None,
                    "font_color" : pygame.Color("white"),
                    "hover_font_color" : None,
                    "clicked_font_color" : None}
        for kwarg in kwargs:
            if kwarg in settings:
                settings[kwarg] = kwargs[kwarg]
            else:
                raise AttributeError("Button has no keyword: {}".format(kwarg))
        self.__dict__.update(settings)

    def render_text(self):
        """Pre render the button text."""
        if self.text:
            if self.hover_font_color:
                color = self.hover_font_color
                self.hover_text = self.font.render(self.text,True,color)
            if self.clicked_font_color:
                color = self.clicked_font_color
                self.clicked_text = self.font.render(self.text,True,color)
            self.text = self.font.render(self.text,True,self.font_color)

    def check_event(self,event):
        """The button needs to be passed events from your program event loop."""
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.on_click(event)
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.on_release(event)

    def on_click(self,event):
        if self.rect.collidepoint(event.pos):
            self.clicked = True
            if not self.call_on_release:
                self.function()
            self.click_s.play()

    def on_release(self,event):
        if self.clicked and self.call_on_release:
            self.function()
        self.clicked = False

    def update(self,surface):
        """Update needs to be called every frame in the main loop."""
        color = self.color
        text = self.text
        if self.clicked and self.clicked_color:
            color = self.clicked_color
            if self.clicked_font_color:
                text = self.clicked_text
        elif self.hovered and self.hover_color:
            color = self.hover_color
            if self.hover_font_color:
                text = self.hover_text
        if not self.transparent:
            surface.fill(pygame.Color("black"),self.rect)
            surface.fill(color,self.rect.inflate(-4,-4))
        if self.text:
            text_rect = text.get_rect(center=self.rect.center)
            surface.blit(text,text_rect)
