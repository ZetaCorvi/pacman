from settings import *
import pygame
from interface_elements import Image, Button
import rus
import eng
from easygui import *
import settings


class GameOver:
    def __init__(self, info_structure):
        self.should_go_to = None
        self.lang = None
        if settings.SELECTED_LANGUAGE == "rus":
            self.lang = rus
        else:
            self.lang = eng
        self.info_structure = info_structure
        self.images = dict()
        self.buttons = dict()
        self.screen = pygame.display.set_mode(SIZE)
        self.create_gameover_panel()
        self.draw_all()
        self.game_saved = False
        if not info_structure.win:
            self.save_score()

    def main_loop(self):
        while not self.should_go_to:
            self.check_events()
            self.draw_all()

    def save_score(self):
        box = multenterbox(self.lang.enter_name, self.lang.score_saving, [self.lang.name], [""])
        if box is not None:
            entered_name = box[0]
            if entered_name is not None:
                    while len(entered_name) < 5:
                        msgbox(self.lang.name_length, "OK")
                        box = multenterbox(self.lang.enter_name, self.lang.score_saving, [self.lang.name], [""])
                        if box is not None:
                            entered_name = box[0]
                            if entered_name is None:
                                break
                        else:
                            entered_name = None
                            break
                    if entered_name:
                        self.save_highscore(entered_name, self.info_structure.score)
                        msgbox(self.lang.score_saved, "OK")
                        self.game_saved = True

    def quit_game(self):
        ask = True
        if not self.game_saved:
            ask = ynbox(self.lang.score_not_saved, self.lang.quit, ["[<F1>]"+self.lang.yes, "[<F2>]"+self.lang.no], default_choice="<F2>"+self.lang.no, cancel_choice="<F2>"+self.lang.no)
        if ask is not None:
            if ask:
                exit()
            else:
                ask_saving = ynbox(self.lang.score_save, self.lang.score_saving, ["[<F1>]"+self.lang.yes, "[<F2>]"+self.lang.no], default_choice="<F2>"+self.lang.no, cancel_choice="<F2>"+self.lang.no)
                if ask_saving is not None:
                    if ask_saving:
                        self.save_score()

    def check_events(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            for i in self.buttons.values():
                if i.active:
                    i.check_event(event)

    def draw_all(self):
        for i in self.images.values():
            if i:
                if type(i) == Image and i.active:
                    self.screen.blit(i.image, i.geometry)
                elif type(i) == list:
                    for j in i:
                        if j:
                            if j.active:
                                self.screen.blit(j.image, j.geometry)
        for i in self.buttons.values():
            if type(i) == Button and i.active:
                i.update(self.screen)
        self.draw_text()
        pygame.display.flip()

    def draw_text(self):
        font = pygame.font.SysFont('Comic Sans MS', 30)
        text = self.lang.your_score + str(self.info_structure.score)
        tmp = font.size(text)
        surface = font.render(text, False, WHITE)
        self.screen.blit(surface, (WIDTH // 2 - tmp[0] // 2, 130))

    def create_gameover_panel(self):
        if self.info_structure.win:
            link = "images/win.png"
        else:
            link = "images/pacman_gameover.png"
        tmp_img = Image(WIDTH / 2 - 216, 50, link, True)
        self.images['gameover_image'] = tmp_img

        message = self.lang.main_menu
        tmp_btn = Button((0, 0, 150, 50), BLACK, self.go_to_main_menu, text=message, **BUTTON_STYLE)
        tmp_btn.rect.center = (WIDTH / 2, 350)
        self.buttons['main_menu_btn'] = tmp_btn
        if self.info_structure.win:
            message = self.lang.next_level
        else:
            message = self.lang.restart
        tmp_btn = Button((0, 0, 250, 80), BLACK, self.restart_game, text=message, **BUTTON_STYLE)
        tmp_btn.rect.center = (WIDTH / 2, 270)
        self.buttons['restart_btn'] = tmp_btn

        message = self.lang.quit
        tmp_btn = Button((0, 0, 150, 50), BLACK, self.quit_game, text=message, **BUTTON_STYLE)
        tmp_btn.rect.center = (WIDTH / 2, 430)
        self.buttons['quit_btn'] = tmp_btn

    def restart_game(self):
        ask = True
        if not self.game_saved:
            ask = ynbox(self.lang.score_not_saved, self.lang.quit, ["[<F1>]"+self.lang.yes, "[<F2>]"+self.lang.no], default_choice="<F2>"+self.lang.no, cancel_choice="<F2>"+self.lang.no)
        if ask is not None:
            if ask:
                self.should_go_to = "game"
                if not self.info_structure.win:
                    self.info_structure.score = 0
            else:
                ask_saving = ynbox(self.lang.score_save, self.lang.score_saving, ["[<F1>]"+self.lang.yes, "[<F2>]"+self.lang.no], default_choice="<F2>"+self.lang.no, cancel_choice="<F2>"+self.lang.no)
                if ask_saving is not None:
                    if ask_saving:
                        self.save_score()

    def go_to_main_menu(self):
        ask = True
        if not self.game_saved:
            ask = ynbox(self.lang.score_not_saved, self.lang.quit, ["[<F1>]"+self.lang.yes, "[<F2>]"+self.lang.no], default_choice="<F2>"+self.lang.no, cancel_choice="<F2>"+self.lang.no)
        if ask is not None:
            if ask:
                self.should_go_to = "main menu"
                self.info_structure.score = 0
                self.info_structure.last_time_tic = 20000
            else:
                ask_saving = ynbox(self.lang.score_save, self.lang.score_saving, ["[<F1>]"+self.lang.yes, "[<F2>]"+self.lang.no], default_choice="<F2>"+self.lang.no, cancel_choice="<F2>"+self.lang.no)
                if ask_saving is not None:
                    if ask_saving:
                        self.save_score()

    @staticmethod
    def save_highscore(player_name, highscore):
        highscores_file = open("highscores.txt", 'a')
        highscores_file.write(player_name + ' ' + str(highscore) + '\n')
