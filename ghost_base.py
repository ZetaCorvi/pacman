from interface_elements import Image
from settings import *
from math import sqrt
from random import randint
import settings


class BaseGhost:

    def __init__(self, x, y, image_link, field, pacman, ghost_type, info_struct):
        self.sprite = Image(x, y, image_link, True, 2, "game_object")
        self.field = field
        self.geometry = self.sprite.geometry
        self.pacman = pacman
        self.speed = info_struct.ghost_speed
        self.info_struct = info_struct
        self.active = True
        self.direction = "right"
        self.cell_y = self.geometry.center[1] // SQUARE_SIZE
        self.cell_x = self.geometry.center[0] // SQUARE_SIZE
        self.last_cell_x = self.geometry.center[0] // SQUARE_SIZE
        self.last_cell_y = self.geometry.center[1] // SQUARE_SIZE
        self.is_cell_checked = False
        self.target_point = 0, 0
        self.movement_mode = "home"
        self.name = ghost_type
        self.last_time = 0
        self.animation_num = 3
        self.white_and_blue_anim_num = 1
        self.white_and_blue_changed_time = 0

    def count_cells_coordinates(self):
        self.cell_y = self.geometry.center[1] // SQUARE_SIZE
        self.cell_x = self.geometry.center[0] // SQUARE_SIZE
        if self.cell_x != self.last_cell_x or self.cell_y != self.last_cell_y:
            self.is_cell_checked = False
        self.last_cell_x = self.cell_x
        self.last_cell_y = self.cell_y

    def put_ghost_in_middle(self, x, y):
        self.geometry.left = SQUARE_SIZE * x + SQUARE_SIZE // 2 - PACMAN_SIZE // 2
        self.geometry.top = SQUARE_SIZE * y + SQUARE_SIZE // 2 - PACMAN_SIZE // 2

    def get_curent_square(self):
        if self.direction == "up" and not self.is_cell_checked:
            if self.geometry.center[1] <= self.field[self.cell_y][self.cell_x].y + SQUARE_SIZE // 2:
                self.put_ghost_in_middle(self.cell_x, self.cell_y)
                self.chose_direction()
                self.is_cell_checked = True
        if self.direction == "down" and not self.is_cell_checked:
            if self.geometry.center[1] >= self.field[self.cell_y][self.cell_x].y + SQUARE_SIZE // 2:
                self.put_ghost_in_middle(self.cell_x, self.cell_y)
                self.chose_direction()
                self.is_cell_checked = True
        if self.direction == "right" and not self.is_cell_checked:
            if self.geometry.center[0] >= self.field[self.cell_y][self.cell_x].x + SQUARE_SIZE // 2:
                self.put_ghost_in_middle(self.cell_x, self.cell_y)
                self.chose_direction()
                self.is_cell_checked = True
        if self.direction == "left" and not self.is_cell_checked:
            if self.geometry.center[0] <= self.field[self.cell_y][self.cell_x].x + SQUARE_SIZE // 2:
                self.put_ghost_in_middle(self.cell_x, self.cell_y)
                self.chose_direction()
                self.is_cell_checked = True

    def get_directions(self):
        dir_list = list()
        if self.field[self.cell_y][self.cell_x - 1].type_of_square != "wall" and self.direction != "right" and self.field[self.cell_y][self.cell_x - 1].type_of_square != "door":
            dir_list.append("left")
        if self.field[self.cell_y][self.cell_x + 1].type_of_square != "wall" and self.direction != "left" and self.field[self.cell_y][self.cell_x + 1].type_of_square != "door":
            dir_list.append("right")
        if self.field[self.cell_y + 1][self.cell_x].type_of_square != "wall" and self.direction != "up" and self.field[self.cell_y + 1][self.cell_x].type_of_square != "door":
            dir_list.append("down")
        if self.field[self.cell_y - 1][self.cell_x].type_of_square != "wall" and self.direction != "down" and self.field[self.cell_y - 1][self.cell_x].type_of_square != "door":
            dir_list.append("up")
        return dir_list

    def chose_direction(self):
        directions = self.get_directions()
        if self.movement_mode == "chase" or self.movement_mode == "scatter" or self.movement_mode == "going_home":
            if len(directions) == 1:
                self.direction = directions[0]
            elif len(directions) >= 2:
                cells = list()
                for i in directions:
                    if i == "up":
                        cell = self.cell_x, self.cell_y - 1
                        cells.append(cell)
                    elif i == "down":
                        cell = self.cell_x, self.cell_y + 1
                        cells.append(cell)
                    elif i == "left":
                        cell = self.cell_x - 1, self.cell_y
                        cells.append(cell)
                    elif i == "right":
                        cell = self.cell_x + 1, self.cell_y
                        cells.append(cell)
                length = list()
                for i in cells:
                    length.append(sqrt(pow(i[0] - self.target_point[0], 2) + pow(i[1] - self.target_point[1], 2)))
                self.direction = directions[length.index(min(length))]
        elif len(directions) > 0 and self.movement_mode == "frightened":
            self.direction = directions[randint(0, len(directions) - 1)]
            # chose random direction

    def shift(self):
        if self.direction == 'up':
            self.geometry.top -= self.speed
        elif self.direction == 'left':
            self.geometry.right -= self.speed
        elif self.direction == 'down':
            self.geometry.top += self.speed
        elif self.direction == 'right':
            self.geometry.right += self.speed
        if self.geometry.right < 0:
            self.geometry.left = WIDTH_CELLS * SQUARE_SIZE
        elif self.geometry.left > WIDTH_CELLS * SQUARE_SIZE:
            self.geometry.right = 0

    def update(self):
        if self.active and self.movement_mode == "frightened":
            self.speed = self.info_struct.ghost_speed - 1
        elif self.active and self.movement_mode == "going_home":
            self.speed = self.speed = self.info_struct.ghost_speed + 2
        elif self.active:
            self.speed = self.speed = self.info_struct.ghost_speed
        if self.active:
            if self.movement_mode != "home" and self.movement_mode != "going_from_home" and self.movement_mode != "going_in_home":
                self.shift()
            elif self.movement_mode == "going_from_home":
                self.going_out()
            elif self.movement_mode == "going_in_home":
                self.going_in()
        if (self.cell_x >= 1) and (self.cell_x <= WIDTH_CELLS - 2) and (self.cell_y >= 0) and (self.cell_y <= HEIGHT_CELLS - 1) and self.movement_mode != "going_from_home" and self.movement_mode != "going_in_home" and self.movement_mode != "home":
            self.get_target_point()
            self.get_curent_square()
        if settings.SCALED_TIME - self.last_time > 50:
                self.get_animation()
                self.last_time = settings.SCALED_TIME
        self.count_cells_coordinates()

    def going_in(self):
        if self.geometry.left != BLINKY_SPAWN_X:
            if BLINKY_SPAWN_X > self.geometry.left:
                self.geometry.left += 1
            elif BLINKY_SPAWN_X < self.geometry.left:
                self.geometry.left -= 1
        elif self.geometry.top < PINKY_SPAWN_Y:
            self.geometry.top += 1
        else:
            self.speed = self.speed = self.info_struct.ghost_speed
            self.movement_mode = "home"

    def going_out(self):
        if self.geometry.left < BLINKY_SPAWN_X and self.geometry.top > BLINKY_SPAWN_Y:
            self.geometry.left += 1
        elif self.geometry.left > BLINKY_SPAWN_X and self.geometry.top > BLINKY_SPAWN_Y:
            self.geometry.left -= 1
        elif self.geometry.top > BLINKY_SPAWN_Y:
            self.geometry.top -= 1
        elif self.geometry.left > BLINKY_SPAWN_X - SQUARE_SIZE // 2:
            self.geometry.left -= 1
        else:
            self.direction = "left"
            self.movement_mode = "chase"
