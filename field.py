from interface_elements import Image
from settings import SQUARE_SIZE


class Field:
    def __init__(self):
        self.score_to_win = 0
        self.squares_array = self.create_map()
        self.images_array = self.create_images_array()
        self.background_map_image = Image(0, 0, "images/map_image.jpg", True, 0, "field")

    def create_map(self):
        tmp_squares_array = []
        read_array = Field.read_map_from_file()
        for i in range(len(read_array)):
            tmp_array = []
            for j in range(len(read_array[i])):
                type_of_square = None
                if read_array[i][j] == "1":
                    type_of_square = "wall"
                elif read_array[i][j] == "0":
                    type_of_square = "small_corn"
                    self.score_to_win += 1
                elif read_array[i][j] == "8":
                    type_of_square = "door"
                elif read_array[i][j] == "2":
                    type_of_square = "big_corn"
                    self.score_to_win += 1
                if type_of_square:
                    tmp_array.append(Square(type_of_square, j * SQUARE_SIZE, i * SQUARE_SIZE))
            tmp_squares_array.append(tmp_array)
        return tmp_squares_array

    def create_images_array(self):
        tmp_images_array = []
        for i in range(len(self.squares_array)):
            for j in range(len(self.squares_array[i])):
                if self.squares_array[i][j].sprite:
                    tmp_images_array.append(self.squares_array[i][j].sprite)
        return tmp_images_array

    @staticmethod
    def read_map_from_file():
        inf_from_file_array = []
        map_file = open("MAP.txt", 'r')
        for line in map_file:
            inf_from_file_array.append(line)
        map_file.close()
        return inf_from_file_array


class Square:
    def __init__(self, type_of_square, x, y):
        self.type_of_square = type_of_square
        self.x = x
        self.y = y
        self.sprite = None
        if type_of_square == "wall":
            self.sprite = None
        elif type_of_square == "door":
            self.sprite = None
        elif type_of_square == "small_corn":
            self.sprite = Image(x, y, "images/small_corn.png", True, 1, "field_object")
        elif type_of_square == "big_corn":
            self.sprite = Image(x, y, "images/big_corn.png", True, 1, "field_object")

