import pygame

from settings import *
from interface_elements import Image, Button
from pacman_class import Pacman
from field import Field
from ghosts import Blinky, Pinky, Inky, Clyde
from soundplayer_class import *
import random
import rus
import eng


class Game:
    def __init__(self, info_structure):
        settings.SCALED_TIME = 0
        settings.UNSCALED_TIME = 0
        settings.TIMESCALE = 1
        self.screen = pygame.display.set_mode(SIZE)
        # state
        self.lang = None
        if settings.SELECTED_LANGUAGE == "rus":
            self.lang = rus
        else:
            self.lang = eng
        self.info_structure = info_structure
        self.images = dict()
        self.buttons = dict()
        self.ghosts = dict()
        self.field = Field()
        self.game_over = False
        self.scatter_mode = False
        self.last_time = 0
        self.wave = 0
        self.pacman = Pacman(PACMAN_SPAWN_X, PACMAN_SPAWN_Y, self.field, self.ghosts, info_structure, False)
        self.pacman.score = self.info_structure.score
        self.images['background_map_images'] = self.field.background_map_image
        self.images['pacman_image'] = self.pacman.sprite
        self.images['field_images'] = self.field.images_array
        self.add_ghosts()
        self.last_lives = self.pacman.lives
        self.pacman.draw_lives()
        self.images["lives"] = self.pacman.list_lives
        self.start_game_sound = SoundPlayer("sound/intro.wav", "effect")
        self.start_game_sound.play()
        self.add_pause_items()
        self.strawberry_spawned = False
        self.cherry_spawned = False
        self.started_time = 0
        self.game_started = False
        self.game_paused = False
        self.layers_count = 5
        self.clock = pygame.time.Clock()
        settings.SCALED_TIME = 0
        settings.UNSCALED_TIME = 0
        # Add your objects here

    def start_game(self):
        self.pacman.active = True
        for i in self.ghosts.values():
            i.active = True

    def add_ghosts(self):
        ghost_blinky = Blinky(BLINKY_SPAWN_X, BLINKY_SPAWN_Y, "images/blinky_right_3.png", self.field.squares_array, self.pacman, "blinky", self.info_structure)
        self.images['ghost_blinky'] = ghost_blinky.sprite
        ghost_pinky = Pinky(PINKY_SPAWN_X, PINKY_SPAWN_Y, "images/pinky_right_3.png", self.field.squares_array, self.pacman, "pinky", self.info_structure)
        self.images['ghost_pinky'] = ghost_pinky.sprite
        ghost_inky = Inky(INKY_SPAWN_X, INKY_SPAWN_Y, "images/inky_right_3.png", self.field.squares_array, self.pacman, ghost_blinky, "inky", self.info_structure)
        self.images['ghost_inky'] = ghost_inky.sprite
        ghost_clyde = Clyde(CLYDE_SPAWN_X, CLYDE_SPAWN_Y, "images/clyde_right_3.png", self.field.squares_array, self.pacman, "clyde", self.info_structure)
        self.images['ghost_clyde'] = ghost_clyde.sprite
        self.ghosts["blinky"] = ghost_blinky
        self.ghosts["pinky"] = ghost_pinky
        self.ghosts["inky"] = ghost_inky
        self.ghosts["clyde"] = ghost_clyde
        for i in self.ghosts.values():
            i.active = True
            i.sprite.active = True

    def add_pause_items(self):

        massage = self.lang.continue_play
        tmp_continue_game_btn = Button((0, 0, 150, 100), BLACK, self.continue_game, True, text=massage, **BUTTON_STYLE)
        tmp_continue_game_btn.rect.center = (WIDTH // 2, HEIGHT // 2 - HEIGHT // 10)
        tmp_continue_game_btn.active = False
        self.buttons["continue_game"] = tmp_continue_game_btn

        massage = self.lang.main_menu
        tmp_main_menu_btn = Button((0, 0, 150, 100), BLACK, self.exit_to_main_menu, True, text=massage, **BUTTON_STYLE)
        tmp_main_menu_btn.rect.center = (WIDTH // 2, HEIGHT // 2 + HEIGHT // 10)
        tmp_main_menu_btn.active = False
        self.buttons["main_menu"] = tmp_main_menu_btn

        self.images["pause_background"] = Image(0, 0, "images/pause_background.png", False, 4)

    def count_time(self):
        settings.SCALED_TIME += self.clock.get_time() * settings.TIMESCALE
        settings.UNSCALED_TIME += self.clock.get_time()

    def main_loop(self):
        while not self.game_over:
            if not self.game_started and settings.SCALED_TIME - self.started_time >= 4200:
                self.game_started = True
                self.start_game()
            self.game_logic()
            self.process_events()
            self.draw()
            self.clock.tick(60)
            self.count_time()
        self.info_structure.score = self.pacman.score
        self.info_structure.win = self.pacman.lives > 0

        if self.info_structure.win:
            self.info_structure.wave_cow += 1
            self.info_structure.score += 500
            self.info_structure.score += 100 * self.pacman.lives
            if self.info_structure.last_time_tic > 5000:
                self.info_structure.last_time_tic -= 2000

    def process_events(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE and not self.game_paused and self.game_started:
                    self.put_game_on_pause()
                elif event.key == pygame.K_ESCAPE and self.game_paused and self.game_started:
                    self.continue_game()
            for i in self.buttons.values():
                if i.active:
                    i.check_event(event)
            if self.pacman.active:
                self.pacman.check_event(event)
            # Add your events here

    def game_logic(self):
        if self.pacman:
            self.pacman.update()
        for i in self.ghosts.values():
            i.update()
            self.set_ghosts_mode(i)
        self.check_pacman_died()
        if self.pacman.eated_corns_count >= 70 and not self.cherry_spawned:
            self.spawn_cherry()
        if self.pacman.eated_corns_count >= 170 and not self.strawberry_spawned:
            self.spawn_strawberry()
        if settings.SCALED_TIME - self.last_time > self.info_structure.last_time_tic:
            self.wave += 1
            if self.wave >= self.info_structure.wave_cow:
                self.scatter_mode = True
                self.wave = 0
            else:
                self.scatter_mode = False
            self.last_time = settings.SCALED_TIME

        if (self.pacman.lives == 0) or (self.pacman.eated_corns_count == self.field.score_to_win):
            self.game_over = True
            self.pacman.enter_eating_mode_sound.stop()
        # Add your game logic here

    def check_pacman_died(self):
        if self.last_lives != self.pacman.lives:
            self.last_lives = self.pacman.lives
            self.ghosts.clear()
            self.add_ghosts()

    def spawn_cherry(self):
        self.cherry_spawned = True
        rand_y = random.randint(0, len(self.field.squares_array) - 1)
        rand_x = random.randint(0, len(self.field.squares_array[rand_y]) - 1)
        tmp_square = self.field.squares_array[rand_y][rand_x]
        while tmp_square.type_of_square == "wall" or tmp_square.type_of_square == "door" or tmp_square.type_of_square == "big_corn":
            rand_y = random.randint(0, len(self.field.squares_array) - 1)
            rand_x = random.randint(0, len(self.field.squares_array[rand_y]) - 1)
            tmp_square = self.field.squares_array[rand_y][rand_x]
        if tmp_square.type_of_square == "small_corn":
            self.field.score_to_win -= 1
        tmp_square.type_of_square = "bonus_cherry"
        tmp_square.sprite.change_image("images/cherry.png")
        tmp_square.sprite.active = True

    def spawn_strawberry(self):
        self.strawberry_spawned = True
        rand_y = random.randint(0, len(self.field.squares_array) - 1)
        rand_x = random.randint(0, len(self.field.squares_array[rand_y]) - 1)
        tmp_square = self.field.squares_array[rand_y][rand_x]
        while tmp_square.type_of_square == "wall" or tmp_square.type_of_square == "door" or tmp_square.type_of_square == "big_corn" or tmp_square.type_of_square == "bonus_cherry":
            rand_y = random.randint(0, len(self.field.squares_array) - 1)
            rand_x = random.randint(0, len(self.field.squares_array[rand_y]) - 1)
            tmp_square = self.field.squares_array[rand_y][rand_x]
        if tmp_square.type_of_square == "small_corn":
            self.field.score_to_win -= 1
        tmp_square.type_of_square = "bonus_strawberry"
        tmp_square.sprite.change_image("images/strawberry.png")
        tmp_square.sprite.active = True

    def put_game_on_pause(self):
        self.game_paused = True
        settings.TIMESCALE = 0
        self.images["pause_background"].active = True
        for i in self.ghosts.values():
            i.active = False
        self.pacman.active = False
        self.buttons["continue_game"].active = True
        self.buttons["main_menu"].active = True

    def continue_game(self):
        self.game_paused = False
        settings.TIMESCALE = 1
        self.images["pause_background"].active = False
        for i in self.ghosts.values():
            i.active = True
        self.pacman.active = True
        self.buttons["continue_game"].active = False
        self.buttons["main_menu"].active = False

    def exit_to_main_menu(self):
        self.pacman.lives = 0

    def set_ghosts_mode(self, ghost):
        if self.pacman.direction != "stop":
            if ghost.movement_mode == "home" or ghost.movement_mode == "going_from_home":
                if ghost.name == "blinky":
                    ghost.movement_mode = "going_from_home"
                elif ghost.name == "pinky":
                    if self.pacman.eated_corns_count >= 1:
                        ghost.movement_mode = "going_from_home"
                elif ghost.name == "inky":
                    if self.pacman.eated_corns_count >= 30:
                        ghost.movement_mode = "going_from_home"
                elif ghost.name == "clyde":
                    if self.pacman.eated_corns_count >= 100:
                        ghost.movement_mode = "going_from_home"
            elif not self.pacman.eating_mode and ghost.movement_mode != "going_home" and ghost.movement_mode != "going_in_home":
                if self.scatter_mode:
                    ghost.movement_mode = "scatter"
                else:
                    ghost.movement_mode = "chase"
            elif ghost.movement_mode == "going_home":
                if abs(ghost.geometry.center[0] - BLINKY_SPAWN_X) / SQUARE_SIZE < 2 and ghost.geometry.top == BLINKY_SPAWN_Y:
                    ghost.speed = 2
                    ghost.movement_mode = "going_in_home"

    def draw(self):
        self.screen.fill(BLACK)
        for i in range(0, self.layers_count):
            self.draw_images(i)
        self.draw_text()
        self.draw_buttons()
        # draw your objects here
        pygame.display.flip()

    def draw_images(self, layer):
        for i in self.images.values():
            if i:
                if type(i) == Image and i.active:
                    if i.layer == layer:
                        self.screen.blit(i.image, i.geometry)
                elif type(i) == list:
                    for j in i:
                        if j:
                            if j.active:
                                if j.layer == layer:
                                    self.screen.blit(j.image, j.geometry)

    def draw_buttons(self):
        for i in self.buttons.values():
            if type(i) == Button and i.active:
                i.update(self.screen)

    def draw_text(self):
        font = pygame.font.SysFont('Comic Sans MS', 30)
        text = self.lang.score + ": " + str(self.pacman.score)
        surface = font.render(text, False, WHITE)
        self.screen.blit(surface, (20, SQUARE_SIZE * HEIGHT_CELLS + 7))
        text = self.lang.lives + ": "
        surface = font.render(text, False, WHITE)
        self.screen.blit(surface, (440, SQUARE_SIZE * HEIGHT_CELLS + 7))

