highscores = "HIGHSCORES"
start = "START"
settings = "SETTINGS"
back = "BACK"
quit = "QUIT"
music_vol = "Music vol."
sfx_vol = "SFX vol."
your_score = "Your score: "
main_menu = "Main menu"
next_level = "Next level"
restart = "Restart"
yes = "Yes"
no = "No"
score_save = "Do you want to save your score?"
score_not_saved = "Are you sure? Your score hasn't been saved"
score_saved = "Your score has been saved successfully"
score_saving = "Score saving"
enter_name = "Enter your name"
name = "Name"
name_length = "Length of your name should be more then 5 symbols"
pause = "PAUSE"
score = "SCORE"
lives = "LIVES"
continue_play = "CONTINUE"
skins = "SKINS"
tutorial = "HOW TO PLAY?"
lang_in_settings = "LANGUAGE: ENG"
game_mode = "GAME MODE"
easy = "EASY"
hard = "HARD"
medium = "MEDIUM"
no_pacman = "NO PACMAN"
name_of_lang = "eng"