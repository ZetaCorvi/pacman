class GameStructure:
    def __init__(self):
        self.score = 0
        self.win = False
        self.wave_cow = 2
        self.last_time_tic = 20000
        self.pacman_speed = 3
        self.ghost_speed = 2
        self.no_pacman_mode = False
        self.game_mode = 2
