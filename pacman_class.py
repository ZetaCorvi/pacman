import pygame
import settings
from settings import *
from interface_elements import Image
from soundplayer_class import SoundPlayer
import settings


class Pacman(object):
    def __init__(self, pos_x, pos_y, field, ghosts, info_structure, active=False):
        self.field = field
        self.ghosts = ghosts
        self.direction = 'stop'
        self.speed = info_structure.pacman_speed
        self.skin = settings.PACMAN_SKIN
        self.sprite = Image(pos_x, pos_y, "images/skins/" + self.skin + "/stop.png", not info_structure.no_pacman_mode, 2, "game_object")
        self.geometry = self.sprite.geometry
        self.cell_x = 0
        self.cell_y = 0
        self.can_tp = True
        self.last_tp_x = None
        self.count_cells_coordinates()
        self.needed_direction = "stop"
        self.active = active
        self.lives = 3
        self.list_lives = list()
        self.score = 0
        self.eating_mode = False
        self.eating_sound = SoundPlayer("sound/pacman_move.wav", "effect")
        self.last_played_eating_sound = -330
        self.animation_num = 1
        self.last_time = 0
        self.eating_mode_started = 0
        self.eating_mode_is_active_for = 6000
        self.eated_corns_count = 0
        self.bonus_sound = SoundPlayer("sound/bonus_eat.wav", "effect")
        self.enter_eating_mode_sound = SoundPlayer("sound/enter_eating_mode.wav", "effect")
        self.ghost_eat_sound = SoundPlayer("sound/ghost_eat.wav", "effect")
        self.death_sound = SoundPlayer("sound/game_over.wav", "effect")
        self.died_time = 0

    def draw_lives(self):
        if self.lives != 0:
            self.list_lives.clear()
            for i in range(self.lives):
                self.list_lives.append(Image(560 + i * 30, SQUARE_SIZE * HEIGHT_CELLS + 15, 'images/lives.png', True, 0))

    def count_cells_coordinates(self):
        self.cell_y = self.geometry.center[1] // SQUARE_SIZE
        self.cell_x = self.geometry.center[0] // SQUARE_SIZE

    def check_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                self.needed_direction = 'up'
            if event.key == pygame.K_a:
                self.needed_direction = 'left'
            if event.key == pygame.K_s:
                self.needed_direction = 'down'
            if event.key == pygame.K_d:
                self.needed_direction = 'right'

    def shift(self):
        if self.direction == 'up':
            self.geometry.top -= self.speed * self.check_collisions()
        elif self.direction == 'left':
            self.geometry.right -= self.speed * self.check_collisions()
        elif self.direction == 'down':
            self.geometry.top += self.speed * self.check_collisions()
        elif self.direction == 'right':
            self.geometry.right += self.speed * self.check_collisions()

    def check_collisions(self, direction=None, put_in_middle=True):
        if direction:
            pass
        else:
            direction = self.direction
        try:
            if direction == "up":
                if self.field.squares_array[self.cell_y - 1][self.cell_x].type_of_square == "wall" or self.field.squares_array[self.cell_y - 1][self.cell_x].type_of_square == "door":
                    if self.geometry.center[1] <= self.field.squares_array[self.cell_y - 1][self.cell_x].y + SQUARE_SIZE + SQUARE_SIZE // 2:
                        if put_in_middle:
                            self.put_pacman_in_middle(self.cell_x, self.cell_y)
                        return 0
            if direction == "down":
                if self.field.squares_array[self.cell_y + 1][self.cell_x].type_of_square == "wall" or self.field.squares_array[self.cell_y + 1][self.cell_x].type_of_square == "door":
                    if self.geometry.center[1] >= self.field.squares_array[self.cell_y + 1][self.cell_x].y - SQUARE_SIZE // 2:
                        if put_in_middle:
                            self.put_pacman_in_middle(self.cell_x, self.cell_y)
                        return 0
            if direction == "left":
                if self.field.squares_array[self.cell_y][self.cell_x - 1].type_of_square == "wall" or self.field.squares_array[self.cell_y][self.cell_x - 1].type_of_square == "door":
                    if self.geometry.center[0] <= self.field.squares_array[self.cell_y][self.cell_x - 1].x + SQUARE_SIZE + SQUARE_SIZE // 2:
                        if put_in_middle:
                            self.put_pacman_in_middle(self.cell_x, self.cell_y)
                        return 0
            if direction == "right":
                if self.field.squares_array[self.cell_y][self.cell_x + 1].type_of_square == "wall" or self.field.squares_array[self.cell_y][self.cell_x + 1].type_of_square == "door":
                    if self.geometry.center[0] >= self.field.squares_array[self.cell_y][self.cell_x + 1].x - SQUARE_SIZE // 2:
                        if put_in_middle:
                            self.put_pacman_in_middle(self.cell_x, self.cell_y)
                        return 0
        except IndexError:
            pass
        return 1

    def put_pacman_in_middle(self, x, y):
        self.geometry.left = SQUARE_SIZE * x + SQUARE_SIZE // 2 - PACMAN_SIZE // 2
        self.geometry.top = SQUARE_SIZE * y + SQUARE_SIZE // 2 - PACMAN_SIZE // 2

    def put_pacman_in_middle_absolute(self, x, y):
        self.geometry.left = x - PACMAN_SIZE // 2
        self.geometry.top = y - PACMAN_SIZE // 2

    def check_if_turning_is_available(self):
        if self.needed_direction == self.direction:
            pass
        else:
            if ((self.geometry.center[0] <= 0) or (self.geometry.center[0] >= 28 * SQUARE_SIZE)) and (self.cell_y == TELEPORT_CELL_Y):
                if self.needed_direction == "left":
                    self.direction = "left"
                    self.sprite.change_image("images/skins/{}/left.png".format(self.skin))
                elif self.needed_direction == "right":
                    self.direction = "right"
                    self.sprite.change_image("images/skins/{}/right.png".format(self.skin))
            elif self.check_collisions(direction=self.needed_direction, put_in_middle=False) == 1:
                if self.direction == "stop":
                    self.direction = self.needed_direction
                elif (self.direction == "up") and (self.needed_direction == "down"):
                    self.direction = self.needed_direction
                    self.sprite.change_image("images/skins/{}/".format(self.skin) + self.needed_direction + ".png")
                elif (self.direction == "left") and (self.needed_direction == "right"):
                    self.direction = self.needed_direction
                    self.sprite.change_image("images/skins/{}/".format(self.skin) + self.needed_direction + ".png")
                elif (self.direction == "down") and (self.needed_direction == "up"):
                    self.direction = self.needed_direction
                    self.sprite.change_image("images/skins/{}/".format(self.skin) + self.needed_direction + ".png")
                elif (self.direction == "right") and (self.needed_direction == "left"):
                    self.direction = self.needed_direction
                    self.sprite.change_image("images/skins/{}/".format(self.skin) + self.needed_direction + ".png")
                elif (self.geometry.center[0] >= self.field.squares_array[self.cell_y][self.cell_x].x + SQUARE_SIZE // 2 - 1) and (self.geometry.center[0] <= self.field.squares_array[self.cell_y][self.cell_x].x + SQUARE_SIZE // 2 + 1) and (self.geometry.center[1] >= self.field.squares_array[self.cell_y][self.cell_x].y + SQUARE_SIZE // 2 - 1) and (self.geometry.center[1] <= self.field.squares_array[self.cell_y][self.cell_x].y + SQUARE_SIZE // 2 + 1):
                    self.put_pacman_in_middle(self.cell_x, self.cell_y)
                    self.direction = self.needed_direction
                    self.sprite.change_image("images/skins/{}/".format(self.skin) + self.needed_direction + ".png")

    def get_animation(self):
        if self.direction == "stop":
            self.sprite.change_image("images/skins/{}/stop.png".format(self.skin))
        elif self.check_collisions(direction=self.direction, put_in_middle=False) == 1:
            self.animation_num += 1
            if self.animation_num == 2 or self.animation_num == 0:
                self.sprite.change_image("images/skins/{}/{}.png".format(self.skin, self.direction))
            elif self.animation_num == 3:
                self.animation_num = -1
                self.sprite.change_image("images/skins/{}/{}_pom.png".format(self.skin, self.direction))
            elif self.animation_num == 1:
                self.sprite.change_image("images/skins/{}/stop.png".format(self.skin))

    def update(self):
        if self.active:
            self.count_cells_coordinates()
            if (self.cell_x <= WIDTH_CELLS - 1) and (self.cell_x >= 0) and (self.direction != "stop"):
                self.eating()
            if settings.SCALED_TIME - self.last_time > 100:
                self.get_animation()
                self.last_time = settings.SCALED_TIME
            if self.eating_mode:
                if settings.SCALED_TIME - self.eating_mode_started >= self.eating_mode_is_active_for:
                    self.eating_mode = False
            self.check_if_turning_is_available()
            self.shift()
            self.check_tp()
            self.check_collision_with_ghosts()

    def check_tp(self):
        if self.geometry.right < 0:
            self.geometry.left = WIDTH_CELLS * SQUARE_SIZE
            self.count_cells_coordinates()
        elif self.geometry.left > WIDTH_CELLS * SQUARE_SIZE:
            self.geometry.right = 0
            self.count_cells_coordinates()

    def eating(self):
        if self.field.squares_array[self.cell_y][self.cell_x].type_of_square == "small_corn":
            self.field.squares_array[self.cell_y][self.cell_x].type_of_square = None
            self.field.squares_array[self.cell_y][self.cell_x].sprite.active = False
            self.score += 10
            self.eated_corns_count += 1
            if settings.SCALED_TIME - self.last_played_eating_sound >= 330:
                self.last_played_eating_sound = settings.SCALED_TIME
                self.eating_sound.play()
        elif self.field.squares_array[self.cell_y][self.cell_x].type_of_square == "big_corn":
            self.enter_eating_mode_sound.stop()
            self.field.squares_array[self.cell_y][self.cell_x].type_of_square = None
            self.field.squares_array[self.cell_y][self.cell_x].sprite.active = False
            self.eating_mode = True
            self.eated_corns_count += 1
            self.enter_eating_mode_sound.play()
            for i in self.ghosts.values():
                if i.movement_mode != "home" and i.movement_mode != "going_home" and i.movement_mode != "going_from_home" and i.movement_mode != "going_in_home":
                    i.movement_mode = "frightened"
                    if i.direction == "up" and i.field[i.cell_y + 1][i.cell_x].type_of_square != "wall":
                        i.direction = "down"
                    elif i.direction == "down" and i.field[i.cell_y - 1][i.cell_x].type_of_square != "wall":
                        i.direction = "up"
                    elif i.direction == "right":
                        if i.cell_x - 1 >= 0:
                            if i.field[i.cell_y][i.cell_x - 1].type_of_square != "wall":
                                i.direction = "left"
                        else:
                            i.direction = "left"
                    elif i.direction == "left":
                        if i.cell_x + 1 < WIDTH_CELLS:
                            if i.field[i.cell_y][i.cell_x + 1].type_of_square != "wall":
                                i.direction = "right"
                        else:
                            i.direction = "right"
            self.eating_mode_started = settings.SCALED_TIME
        elif self.field.squares_array[self.cell_y][self.cell_x].type_of_square == "bonus_cherry":
            self.field.squares_array[self.cell_y][self.cell_x].type_of_square = None
            self.field.squares_array[self.cell_y][self.cell_x].sprite.active = False
            self.score += 100
            self.bonus_sound.play()
            if settings.SCALED_TIME - self.last_played_eating_sound >= 330:
                self.last_played_eating_sound = settings.SCALED_TIME
                self.eating_sound.play()
        elif self.field.squares_array[self.cell_y][self.cell_x].type_of_square == "bonus_strawberry":
            self.field.squares_array[self.cell_y][self.cell_x].type_of_square = None
            self.field.squares_array[self.cell_y][self.cell_x].sprite.active = False
            self.score += 300
            self.bonus_sound.play()
            if settings.SCALED_TIME - self.last_played_eating_sound >= 330:
                self.last_played_eating_sound = settings.SCALED_TIME
                self.eating_sound.play()

    def check_collision_with_ghosts(self):
        for i in self.ghosts.values():
            if i.movement_mode != "going_home":
                if i.cell_x == self.cell_x and i.cell_y == self.cell_y and self.direction != "stop" and i.active:
                    if i.movement_mode == "chase" or i.movement_mode == "scatter":
                        self.lives -= 1
                        self.eating_mode = False
                        if self.lives >= 0:
                            self.died_time = settings.SCALED_TIME
                            self.enter_eating_mode_sound.stop()
                            self.list_lives[-(3 - self.lives)].active = False
                            self.death_sound.play()
                            while settings.SCALED_TIME - self.died_time < 1500:
                                pygame.time.wait(16)
                                settings.SCALED_TIME += 16
                            self.geometry.left = PACMAN_SPAWN_X
                            self.geometry.top = PACMAN_SPAWN_Y
                            self.count_cells_coordinates()
                            self.sprite.change_image("images/pacman_stop.png")
                            self.direction = "stop"
                            self.needed_direction = "stop"
                    else:
                        if i.movement_mode == "frightened":
                            self.score += 200
                            self.ghost_eat_sound.play()
                            i.movement_mode = "going_home"
